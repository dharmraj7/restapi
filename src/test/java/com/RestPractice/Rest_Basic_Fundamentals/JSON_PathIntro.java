package com.RestPractice.Rest_Basic_Fundamentals;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.testng.annotations.Test;

import io.restassured.path.json.JsonPath;

public class JSON_PathIntro {

	@Test
	public void jsonPathDemo()
	{
		String json="{\r\n" + 
				"    \"firstname\": \"Sally\",\r\n" + 
				"    \"lastname\": \"Brown\",\r\n" +
				"}";
		//Get JSON Path instance of given josn document
		JsonPath jsonPath=new JsonPath(json);
		String firstName=jsonPath.getString("firstname");
		//System.out.println(firstName);
		
		Object fname=jsonPath.get("firstname");
		//System.out.println(fname);
		
		//System.out.println((Object)jsonPath.get("$"));
		//System.out.println(jsonPath.getString("$"));
		System.out.println((char[]) jsonPath.get());
		//System.out.println(jsonPath.getString(""));
	}
	
	@Test
	public void nastedJson()
	{
		String jsonObject="{\r\n" + 
				"    \"name\": \"John\",\r\n" + 
				"    \"age\": 22,\r\n" + 
				"    \"hobby\": {\r\n" + 
				"	\"reading\" : true,\r\n" + 
				"	\"gaming\" : false,\r\n" + 
				"	\"sport\" : \"football\"\r\n" + 
				"    },\r\n" + 
				"    \"class\" : [\"JavaScript\", \"HTML\", \"CSS\"]\r\n" + 
				"}";
		JsonPath jp=new JsonPath(jsonObject);
		String sportName=jp.getString("hobby.sport");
		System.out.println("Sport name "+sportName);
		
		Object hobby=jp.get("hobby");
		System.out.println(hobby);
	}

}
