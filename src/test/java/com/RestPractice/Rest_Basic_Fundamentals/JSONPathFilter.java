package com.RestPractice.Rest_Basic_Fundamentals;

import java.io.File;
import java.util.List;

import io.restassured.path.json.JsonPath;

public class JSONPathFilter {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String filePath= System.getProperty("user.dir")+"\\resources\\payLoad2.json";
		File jsonArrayFile=new File(filePath);
		JsonPath jsonPath=new JsonPath(jsonArrayFile);
		//get the email id for 0th index record
		System.out.println(jsonPath.getString("data[0].Email"));
		
		//get all the first name present in this JSON Array and print
		
		List<String> allFirstName = jsonPath.getList("data.FirstName");
		System.out.println(allFirstName);
		
		//go one by one set of array and display info
		
		List<String> genderWiseFirstNameList=jsonPath.getList("data.findAll{it.Gender=='Female'}");
		System.out.println(genderWiseFirstNameList);
		
		String getEmailByFirst_LastName = jsonPath.getString("data.find{it.FirstName=='Dharmraj' & it.LastName=='Bhapkar'}.Email");
		System.out.println("Mail Id as per First and Last Name is :"+getEmailByFirst_LastName);
		
		

		
		String getContactByFirst_LastName = jsonPath.getString("data.find{it.FirstName=='Dharmraj' | it.LastName=='Bhapkar'}.Contact");
		System.out.println("Contatct number as per First and Last Name is :"+getContactByFirst_LastName);
		
		System.out.println("Display Id >=103  :  "+ jsonPath.getList("data.findAll{it.Id>=103}.FirstName"));
		
	}

}
