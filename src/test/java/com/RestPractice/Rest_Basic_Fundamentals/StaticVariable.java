package com.RestPractice.Rest_Basic_Fundamentals;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

public class StaticVariable {
	
	@BeforeTest
	public void setUp()
	{
		RestAssured.baseURI="https://restful-booker.herokuapp.com/";
		RestAssured.basePath="booking";
		System.out.println("In Setup");
		RestAssured.requestSpecification=RestAssured.given().log().all();
		RestAssured.responseSpecification=RestAssured.expect().statusCode(200);
	}
	@Test
	public void createBooking1()
	{
		RestAssured.
		given()
	    .body("{\r\n" + 
							"    \"firstname\" : \"Dharmraj\",\r\n" + 
							"    \"lastname\" : \"Bhapkar\",\r\n" + 
							"    \"totalprice\" : 111,\r\n" + 
							"    \"depositpaid\" : true,\r\n" + 
							"    \"bookingdates\" : {\r\n" + 
							"        \"checkin\" : \"2021-01-01\",\r\n" + 
							"        \"checkout\" : \"2021-02-02\"\r\n" + 
							"    },\r\n" + 
							"    \"additionalneeds\" : \"Breakfast\"\r\n" + 
							"}")
					.contentType(ContentType.JSON)
					.when()
					.post()
					.then()
					.log()
					.all();
						 
	}
	@Test
	public void createBooking2()
	{
		RestAssured.
		given().
		body("{\r\n" + 
				"    \"firstname\" : \"Draj\",\r\n" + 
				"    \"lastname\" : \"Bhapkar\",\r\n" + 
				"    \"totalprice\" : 9876,\r\n" + 
				"    \"depositpaid\" : true,\r\n" + 
				"    \"bookingdates\" : {\r\n" + 
				"        \"checkin\" : \"2021-01-01\",\r\n" + 
				"        \"checkout\" : \"2021-02-02\"\r\n" + 
				"    },\r\n" + 
				"    \"additionalneeds\" : \"Breakfast\"\r\n" + 
				"}")
				.contentType(ContentType.JSON)
				.when()
				.post().then()
				.log().all();
	}
}
