package com.RestPractice.Rest_Basic_Fundamentals;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class StaticVariable2 {
	

	@Test
	public void createBooking1()
	{
		RestAssured.
		given().basePath("auth").
					body("{\r\n" + 
							"    \"username\" : \"admin\",\r\n" + 
							"    \"password\" : \"password123\"\r\n" + 
							"}")
					.contentType(ContentType.JSON)
					.when()
					.post()
					.then()
					.log()
					.all();
	}

}
