package com.RestPractice.Rest_Basic_Fundamentals;

import java.util.List;

import io.restassured.path.json.JsonPath;

public class JasonPathWithJsonArray {

	public static void main(String[] args) {
		
		//get simple JSON array element and array size
		//simpleJSONArray();
		
		//get JSON array within array and get the element and array size
		//JSON_ArrayWithinArray();    
	
		//Nested array Example
		JSONPathWithJSONArray_Nasted();
		
	}
	
	public static void simpleJSONArray()
	{
		String jasonArray="[\r\n" + 
				"  \"10\",\r\n" + 
				"  \"20\",\r\n" + 
				"  \"30\",\r\n" + 
				"  \"40\",\r\n" + 
				"  \"50\"\r\n" + 
				"]";
		
		JsonPath jsonPath=new JsonPath(jasonArray);
		String ss=jsonPath.getString("[8]");
		System.out.println(ss);
		System.out.println((char[]) jsonPath.get("[2]"));
		
		//size of Json array
		System.out.println("Size of JAON Array : "+jsonPath.getList("$").size());
	
	}
	
	public static void JSON_ArrayWithinArray()
	{
		String jasonArray="[\r\n" + 
				"  [\r\n" + 
				"    \"10\",\r\n" + 
				"    \"20\",\r\n" + 
				"    \"30\",\r\n" + 
				"    \"40\",\r\n" + 
				"    \"50\"\r\n" + 
				"  ],\r\n" + 
				"  [\r\n" + 
				"    \"101\",\r\n" + 
				"    \"201\",\r\n" + 
				"    \"301\",\r\n" + 
				"    \"401\",\r\n" + 
				"    \"501\",\r\n" + 
				"    \"601\"\r\n" + 
				"  ]\r\n" + 
				"]";
		
		JsonPath jsonPath=new JsonPath(jasonArray);
		String ss=jsonPath.getString("[0][1]");
		System.out.println(ss);
		System.out.println((char[]) jsonPath.get("[1][2]"));
		
		//size of Json array
		System.out.println("Size of JAON Array : "+jsonPath.getList("$").size());
		//Internal JSON Array size
		List<Object> internalList = (List<Object>) jsonPath.getList("$").get(1);
		System.out.println("Size of internal array : "+internalList.size());
		
	
	}
	
	
	public static void JSONPathWithJSONArray_Nasted()
	
	{
		String jsonArray="[\r\n" + 
				"  {\r\n" + 
				"    \"FirstName\": \"Dharmraj\",\r\n" + 
				"    \"LastName\": \"Bhapkar\",\r\n" + 
				"    \"Age\": 28,\r\n" + 
				"    \"address\": [\r\n" + 
				"      {\r\n" + 
				"        \"City\": \"Nashik\",\r\n" + 
				"        \"Country\": \"India\"\r\n" + 
				"      },\r\n" + 
				"      {\r\n" + 
				"        \"City\": \"Delhi\",\r\n" + 
				"        \"Country\": \"India\"\r\n" + 
				"      }\r\n" + 
				"    ]\r\n" + 
				"  },\r\n" + 
				"  {\r\n" + 
				"    \"FirstName\": \"Dixon\",\r\n" + 
				"    \"LastName\": \"Davis\",\r\n" + 
				"    \"Age\": 32,\r\n" + 
				"    \"address\": [\r\n" + 
				"      {\r\n" + 
				"        \"City\": \"Pune\",\r\n" + 
				"        \"Country\": \"India\"\r\n" + 
				"      },\r\n" + 
				"      {\r\n" + 
				"        \"City\": \"Kerla\",\r\n" + 
				"        \"Country\": \"India\"\r\n" + 
				"      }\r\n" + 
				"    ]\r\n" + 
				"  }\r\n" + 
				"]";
		JsonPath jsonPath=new JsonPath(jsonArray);
		String getCity = jsonPath.getString("[1].address[0].City");
		System.out.println("City  : "+getCity);
		
		System.out.println("Display address details of City "+jsonPath.getList("[0].address.City"));
		System.out.println("Display address details of City "+jsonPath.getList("address.City"));
		
	}
}
