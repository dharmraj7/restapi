package com.RestPractice.Rest_Basic_Fundamentals;
import org.apache.juneau.http.annotation.Body;
import org.hamcrest.Matchers;
import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;



public class WithoutInlineAssertions {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		assertionWithAsset();
		
		System.out.println("************************************************");
		//assertionWithMatchers();
		
	}
	public static void assertionWithAsset()
	{
		String jsonResponse="";
		jsonResponse=RestAssured
				.given()
				.log()
				.all()
				.baseUri("https://restful-booker.herokuapp.com/auth")
				.body("{\r\n" + 
						"    \"username\" : \"admin\",\r\n" + 
						"    \"password\" : \"password123\"\r\n" + 
						"}")
				.contentType(ContentType.JSON)
				.when()
				.post()
				.then()
				.log().all()
				.extract()
				.asString();
		
		JsonPath jsonPath=new JsonPath(jsonResponse);
		
		Assert.assertNotNull(jsonPath.get("token"));
	}
	
	

}
