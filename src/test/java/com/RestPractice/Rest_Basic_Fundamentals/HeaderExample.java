  package com.RestPractice.Rest_Basic_Fundamentals;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.meta.When;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Header;

public class HeaderExample {
/*
	@Test
	public void passHeader1()
	{
		RestAssured.given()
		.log()
		.all()
		.header("Header1","Value1")
		.header("Header2","Value1","Value2","Value3")
		.when().get();
	}
	@Test
	public void passHeader2()
	{
		RestAssured.given()
		.log()
		.all()
		.header("Header1","Value1")
		.header("Header2","Value2")
		.when().get();
	}
	@Test
	public void passHeader3()
	{
		Header header=new Header("Header1","Value1");
		RestAssured.given()
		.log()
		.all()
		.header(header)
		
		.when().get();
	}*/
	@Test
	public void passHeader4()
	{
	Map<String, String> headerMap=new HashMap<String, String>();
		headerMap.put("h1", "v1");
		headerMap.put("h2", "v2");
		headerMap.put("h3", "v3");
		headerMap.put("h4", "v4");
		
		RestAssured.given()
		.log()
		.all().
		//.headers("h1","v1","h2","v2","h3","v3")
		headers(headerMap)
		.when().get();
	}
}
