package com.RestPractice.Rest_Basic_Fundamentals;

import org.hamcrest.Matchers;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class InlineAssertion {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		assertionWithMatchers_Post();
		System.out.println("*****************************************************");
		inlineAssertionWithMatchers_Get("https://restful-booker.herokuapp.com/booking");
	}
	public static void assertionWithMatchers_Post()
	{
	RestAssured
				.given()
				.log()
				.all()
				.baseUri("https://restful-booker.herokuapp.com/auth")
				.body("{\r\n" + 
						"    \"username\" : \"admin\",\r\n" + 
						"    \"password\" : \"password123\"\r\n" + 
						"}")
				.contentType(ContentType.JSON)
				.when()
				.post()
				.then()
				.log().all()
				.body("token", Matchers.notNullValue())
				.body("token.length()", Matchers.is(15))
				.body("token.length()", Matchers.equalTo(15))
				.body("token", Matchers.matchesRegex("^[a-z0-9]+$"));
	}
	public static void inlineAssertionWithMatchers_Get(String url)
	{
		RestAssured.given().baseUri(url)
		.when()
		.get()
		.then()
		.log()
		.all()
		.body("bookingid", Matchers.hasItems(9,10));
		
	}
}
