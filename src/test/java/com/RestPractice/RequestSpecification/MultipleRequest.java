package com.RestPractice.RequestSpecification;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

public class MultipleRequest {

	RequestSpecification httpRequest;
	@BeforeClass
	public void reqSpecification()
	{
		httpRequest=RestAssured.given();
		httpRequest.log().all()
		.baseUri("https://restful-booker.herokuapp.com/")
		
		.header("Content-Type", "application/json");
		
	}
	
	@Test
	public void postRequest()
	{
		/*RestAssured.
		given().log().all().
			baseUri("https://restful-booker.herokuapp.com/").
				basePath("booking").*/
		RestAssured.given().spec(httpRequest).basePath("booking")
					.body("{\r\n" + 
							"    \"firstname\" : \"john\",\r\n" + 
							"    \"lastname\" : \"charles\",\r\n" + 
							"    \"totalprice\" : 200,\r\n" + 
							"    \"depositpaid\" : true,\r\n" + 
							"    \"bookingdates\" : {\r\n" + 
							"        \"checkin\" : \"2021-12-12\",\r\n" + 
							"        \"checkout\" : \"2021-12-12\"\r\n" + 
							"    },\r\n" + 
							"    \"additionalneeds\" : \"Dinner\"\r\n" + 
							"}")
					//.contentType(ContentType.JSON)
					.when().post()
					.then()
					.log()
					.all()
						.statusCode(200);
	}
	
	@Test
	public void putRequest()
	{
		RestAssured.given().spec(httpRequest)	
		.header("Authorization","Basic YWRtaW46cGFzc3dvcmQxMjM=").basePath("booking/1")
		.body("{\r\n" + 
				"    \"firstname\" : \"John\",\r\n" + 
				"    \"lastname\" : \"Charles\",\r\n" + 
				"    \"totalprice\" : 400,\r\n" + 
				"    \"depositpaid\" : true,\r\n" + 
				"    \"bookingdates\" : {\r\n" + 
				"        \"checkin\" : \"2021-12-12\",\r\n" + 
				"        \"checkout\" : \"2021-12-12\"\r\n" + 
				"    },\r\n" + 
				"    \"additionalneeds\" : \"Lunch\"\r\n" + 
				"}")
		.when().put().then().log().all().statusCode(200);
	}
}
