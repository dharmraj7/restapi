package com.RestPractice.RequestSpecification;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

public class RequestResponseSpecification_Ex {

	
	@Test
	public void Request_ResponseSpecification_Type1()
	{
		
		RequestSpecification httpRequest = RestAssured.given();
		httpRequest
		.log()
		.all()
		.baseUri("https://restful-booker.herokuapp.com/")
		.basePath("booking")
		.header("Content-Type", "application/json");
			
		
		ResponseSpecification responseSpecification = RestAssured.expect();
		responseSpecification.statusCode(200);
		responseSpecification.contentType(ContentType.JSON);
		responseSpecification.time(Matchers.lessThan(5000L));

		
		RestAssured
		.given(httpRequest)
		//.spec(httpRequest)
					.body("{\r\n" + 
							"    \"firstname\" : \"Dharmraj\",\r\n" + 
							"    \"lastname\" : \"Bhapkar\",\r\n" + 
							"    \"totalprice\" : 111,\r\n" + 
							"    \"depositpaid\" : true,\r\n" + 
							"    \"bookingdates\" : {\r\n" + 
							"        \"checkin\" : \"2021-01-01\",\r\n" + 
							"        \"checkout\" : \"2021-02-02\"\r\n" + 
							"    },\r\n" + 
							"    \"additionalneeds\" : \"Breakfast\"\r\n" + 
							"}")
					.post()
					.then()
					.log()
					.all()
					.spec(responseSpecification);
	}
	
	@Test
	public void requestResponseSpecification_Type2()
	{
		
		RequestSpecification httpRequest = RestAssured.given();
		httpRequest
		.log()
		.all()
		.baseUri("https://restful-booker.herokuapp.com/")
		.basePath("booking")
		.body("{\r\n" + 
				"    \"firstname\" : \"Dharmraj\",\r\n" + 
				"    \"lastname\" : \"Bhapkar\",\r\n" + 
				"    \"totalprice\" : 111,\r\n" + 
				"    \"depositpaid\" : true,\r\n" + 
				"    \"bookingdates\" : {\r\n" + 
				"        \"checkin\" : \"2021-01-01\",\r\n" + 
				"        \"checkout\" : \"2021-02-02\"\r\n" + 
				"    },\r\n" + 
				"    \"additionalneeds\" : \"Breakfast\"\r\n" + 
				"}")
		.header("Content-Type", "application/json");
			
		
		ResponseSpecification responseSpecification = RestAssured.expect();
		responseSpecification.statusCode(200);
		responseSpecification.contentType(ContentType.JSON);
		responseSpecification.time(Matchers.lessThan(5000L));

		RestAssured.given(httpRequest, responseSpecification)
		.post()
		.then()
			.log()
			.all();
	}
}
