package com.RestPractice.RequestSpecification;

import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

public class RequestResponseSpecification_Ex2 {
	RequestSpecification requestSpecification ;
	ResponseSpecification responseSpecification ;
	@BeforeClass
	public void setUp()
	{
		//Request Specification common delcaration
		requestSpecification = RestAssured.given()
		.log()
		.all()
		.baseUri("https://restful-booker.herokuapp.com/")
		.basePath("booking")
		.body("{\r\n" + 
				"    \"firstname\" : \"Marvel\",\r\n" + 
				"    \"lastname\" : \"Jack\",\r\n" + 
				"    \"totalprice\" : 700,\r\n" + 
				"    \"depositpaid\" : true,\r\n" + 
				"    \"bookingdates\" : {\r\n" + 
				"        \"checkin\" : \"2021-01-01\",\r\n" + 
				"        \"checkout\" : \"2021-02-02\"\r\n" + 
				"    },\r\n" + 
				"    \"additionalneeds\" : \"Dinner\"\r\n" + 
				"}")
		.header("Content-Type", "application/json");
		
		
		//Response Specification common delcaration
		responseSpecification = RestAssured.expect();
		responseSpecification.statusCode(200);
		responseSpecification.contentType(ContentType.JSON);
		responseSpecification.time(Matchers.lessThan(5000L));
		
	}
	@Test
	public void Request_ResponseSpecification_Type1()
	{
		RestAssured
		.given(requestSpecification)
		//.spec(httpRequest
		.post()
		.then()
			.log()
			.all()
			.spec(responseSpecification);
	}
	
	@Test
	public void requestResponseSpecification_Type2()
	{
		
		RestAssured.given(requestSpecification, responseSpecification)
		.post()
		.then()
			.log()
			.all();
	}
}
