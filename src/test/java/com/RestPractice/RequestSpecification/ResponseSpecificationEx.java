 package com.RestPractice.RequestSpecification;

import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import io.restassured.specification.ResponseSpecification;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class ResponseSpecificationEx {
	ResponseSpecification responseSpecification;
	@BeforeClass
	public void setUp_responseSpecifiction()
	{ 
		responseSpecification=RestAssured.expect();
		responseSpecification.statusCode(200);
		responseSpecification.contentType(ContentType.JSON);
		responseSpecification.time(Matchers.lessThan(5000L));
	}
	
	@Test
	public void createRequest()
	{
		RestAssured.
		given().log().all().
			baseUri("https://restful-booker.herokuapp.com/").
				basePath("booking").
					body("{\r\n" + 
							"    \"firstname\" : \"Dharmraj\",\r\n" + 
							"    \"lastname\" : \"Bhapkar\",\r\n" + 
							"    \"totalprice\" : 111,\r\n" + 
							"    \"depositpaid\" : true,\r\n" + 
							"    \"bookingdates\" : {\r\n" + 
							"        \"checkin\" : \"2021-01-01\",\r\n" + 
							"        \"checkout\" : \"2021-02-02\"\r\n" + 
							"    },\r\n" + 
							"    \"additionalneeds\" : \"Breakfast\"\r\n" + 
							"}")
					.contentType(ContentType.JSON).when().post()
					.then()
					.log()
					.all()
					.spec(responseSpecification);
						   
	}
	@Test
	public void createRequest2()
	{

		RestAssured.
		given().log().all().
			baseUri("https://restful-booker.herokuapp.com/").
				basePath("booking").
					body("{\r\n" + 
							"    \"firstname\" : \"Pablo\",\r\n" + 
							"    \"lastname\" : \"david\",\r\n" + 
							"    \"totalprice\" : 600,\r\n" + 
							"    \"depositpaid\" : true,\r\n" + 
							"    \"bookingdates\" : {\r\n" + 
							"        \"checkin\" : \"2021-01-01\",\r\n" + 
							"        \"checkout\" : \"2021-02-02\"\r\n" + 
							"    },\r\n" + 
							"    \"additionalneeds\" : \"Breakfast\"\r\n" + 
							"}")
					.contentType(ContentType.JSON).when().post()
					.then()
					.log()
					.all()
					.spec(responseSpecification);   
	}
	
	
}
