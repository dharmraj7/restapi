package com.RestPractice.ResponseExample;

import java.util.concurrent.TimeUnit;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class Extraction_AssertionWith_Response {

	@Test
	public void getResponseTimeByCreateRequest()
	{
		
		Response response=(Response) RestAssured.
		given().log().all().
		baseUri("https://restful-booker.herokuapp.com/").
			basePath("booking").
				body("{\r\n" + 
						"    \"firstname\" : \"Dharmraj\",\r\n" + 
						"    \"lastname\" : \"Bhapkar\",\r\n" + 
						"    \"totalprice\" : 5030,\r\n" + 
						"    \"depositpaid\" : Fale,\r\n" + 
						"    \"bookingdates\" : {\r\n" + 
						"        \"checkin\" : \"2021-01-01\",\r\n" + 
						"        \"checkout\" : \"2021-02-02\"\r\n" + 
						"    },\r\n" + 
						"    \"additionalneeds\" : \"Lunch\"\r\n" + 
						"}")
				.contentType(ContentType.JSON)
			//	.when()
				.post();
		
			long responseTimeInMS = response.time();
			System.out.println("Response Time in Milli Seconds: "+responseTimeInMS );
			
			long responseTimeInSecond = response.timeIn(TimeUnit.SECONDS);
			System.out.println("Response Time in Seconds: "+responseTimeInSecond );
		
			//response.then().time(Matchers.lessThan(4000L));
			response.then().time(Matchers.greaterThan(1000L));
			response.then().time(Matchers.both(Matchers.greaterThan(1000L))
					.and(Matchers.lessThan(4500L)));
			response.then().time(Matchers.lessThan(2L), TimeUnit.SECONDS);
}
}
