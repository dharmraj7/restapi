package com.RestPractice.Simple_CRUD_Operation;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class ExtractResponseAsString {

	@Test
	public void getExtractResponse()

	{
		String responseBody=RestAssured
		.given()
		.baseUri("https://restful-booker.herokuapp.com/")
		.basePath("booking")
			.body("{\r\n" + 
					"    \"firstname\" : \"Vince\",\r\n" + 
					"    \"lastname\" : \"Chapple\",\r\n" + 
					"    \"totalprice\" : 111,\r\n" + 
					"    \"depositpaid\" : true,\r\n" + 
					"    \"bookingdates\" : {\r\n" + 
					"        \"checkin\" : \"2021-11-11\",\r\n" + 
					"        \"checkout\" : \"2021-11-11\"\r\n" + 
					"    },\r\n" + 
					"    \"additionalneeds\" : \"Breakfast\"\r\n" + 
					"}")
			.contentType(ContentType.JSON)
		.when()
		.post()
		.then()
		.extract()
		.body()
		.asPrettyString();
		
		System.out.println(responseBody.toString());
		
	}
}
