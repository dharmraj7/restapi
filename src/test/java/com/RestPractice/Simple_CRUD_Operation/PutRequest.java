package com.RestPractice.Simple_CRUD_Operation;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
//Update operation
public class PutRequest {
	
	@Test
	public void putRequest()
	{
		RestAssured.given()
		.log().all()
		.baseUri("https://restful-booker.herokuapp.com/").
		basePath("booking/1")
		.header("Content-Type", "application/json").header("Authorization","Basic YWRtaW46cGFzc3dvcmQxMjM=")		
		.body("{\r\n" + 
				"    \"firstname\" : \"Draj\",\r\n" + 
				"    \"lastname\" : \"Bhapkar\",\r\n" + 
				"    \"totalprice\" : 111,\r\n" + 
				"    \"depositpaid\" : true,\r\n" + 
				"    \"bookingdates\" : {\r\n" + 
				"        \"checkin\" : \"2021-01-09\",\r\n" + 
				"        \"checkout\" : \"2021-02-09\"\r\n" + 
				"    },\r\n" + 
				"    \"additionalneeds\" : \"Lunch\"\r\n" + 
				"}")
		.when().put().then().log().all().statusCode(200);
	}

}
