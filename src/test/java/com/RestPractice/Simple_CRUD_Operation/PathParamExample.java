package com.RestPractice.Simple_CRUD_Operation;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.Test;

import io.restassured.RestAssured;

public class PathParamExample {

	@Test(enabled=false)
	public void getRequestWithPathParam()
	{
		RestAssured.given().log().all()
		.baseUri("https://restful-booker.herokuapp.com/")
		.basePath("{basePath}/{bookingId}")
		.pathParam("basePath", "booking")
		.pathParam("bookingId", 2)
		.when()
		.get()
		.then()
		.log()
		.all()
		.statusCode(200);
	}
	
	@Test
	public void getRequestWithMAP()
	{
		Map<String, Object> pathParameter=new HashMap<String, Object>();
		pathParameter.put("basePath", "booking");
		pathParameter.put("bookingId", 2);
		
		RestAssured.given()
		.log()
		.all()
		.baseUri("https://restful-booker.herokuapp.com/")
		.basePath("{basePath}/{bookingId}")
		.pathParams(pathParameter)
		.when()
			.get()
		.then()
		.log()
		.all()
			.statusCode(200);
	}
	
	@Test(enabled=false)
	public void getRequestWithPathParam1()
	{
		RestAssured.given().log().all()
		.baseUri("https://restful-booker.herokuapp.com/")
		//.basePath("{basePath}/{bookingId}")
		//.pathParam("basePath", "booking")
		//.pathParam("bookingId", 2)
		.when()
		.get("{basePath}/{bookingId}", "booking",2)
		.then()
		.log()
		.all()
		.statusCode(200);
	}
}
