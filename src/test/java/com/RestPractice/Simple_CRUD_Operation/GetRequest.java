package com.RestPractice.Simple_CRUD_Operation;

import org.apache.http.protocol.HttpRequestExecutor;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class GetRequest {

	@Test
	public void getRequestWithBDD() {
		RestAssured.given()
		.baseUri("https://restful-booker.herokuapp.com/")
			.basePath("booking/{id}")
				.pathParam("id", 11)
				.when()
				.get()
				.then()
				.log()
				.all()
					.statusCode(200);

	}

	@Test(enabled=false)
	public void getRequest() {
		RequestSpecification httpRequest = RestAssured.given();
		httpRequest.baseUri("https://restful-booker.herokuapp.com/");
		httpRequest.basePath("booking/{id}");
		httpRequest.pathParam("id", 11);

		Response response = httpRequest.get();
		// System.out.println(response.prettyPrint());
		String responseBody = response.prettyPrint();

		System.out.println("Response Body is => " + responseBody);

		// Verifying the status code
		int actStatusCode = response.getStatusCode();
		Assert.assertEquals(200, actStatusCode, "Status Code returened is not as expcted");

		httpRequest.log().all();

	}
}
