package com.RestPractice.Simple_CRUD_Operation;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class PatchRequest {

	
	@Test
	public void patchRequest()
	{
		RestAssured
		.given()
		.baseUri("https://restful-booker.herokuapp.com/")
		.basePath("booking/12")
		.body("{\r\n" + 
				"    \"firstname\" : \"Mora\",\r\n" + 
				"    \"lastname\" : \"Brown\"\r\n" + 
				"}")
		.contentType(ContentType.JSON)
		.header("Authorization", "Basic YWRtaW46cGFzc3dvcmQxMjM=")
		.when()
		.patch()
		.then()
		.log()
		.all()
		.statusCode(200);
	}
}
