package com.RestPractice.Simple_CRUD_Operation;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import okhttp3.internal.http2.Http2Stream;

public class PostRequest {

	@Test
	public void portRequestWithBDD()
	{
		RestAssured.
		given().log().all().
			baseUri("https://restful-booker.herokuapp.com/").
				basePath("booking").
					body("{\r\n" + 
							"    \"firstname\" : \"Dharmraj\",\r\n" + 
							"    \"lastname\" : \"Bhapkar\",\r\n" + 
							"    \"totalprice\" : 111,\r\n" + 
							"    \"depositpaid\" : true,\r\n" + 
							"    \"bookingdates\" : {\r\n" + 
							"        \"checkin\" : \"2021-01-01\",\r\n" + 
							"        \"checkout\" : \"2021-02-02\"\r\n" + 
							"    },\r\n" + 
							"    \"additionalneeds\" : \"Breakfast\"\r\n" + 
							"}")
					.contentType(ContentType.JSON).when().post()
					.then()
					.log()
					.all()
						.statusCode(200);
	}
	

	@Test(enabled=false)
	public void postRequestWithDiffMethod()
	{

		RequestSpecification httpRequest=RestAssured.given();
		httpRequest= httpRequest.log().all();
		
		httpRequest.baseUri("https://restful-booker.herokuapp.com/");
		httpRequest.basePath("booking");
		httpRequest.contentType(ContentType.JSON);
		httpRequest.body("{\r\n" + 
				"    \"firstname\" : \"Dharmraj\",\r\n" + 
				"    \"lastname\" : \"Bhapkar\",\r\n" + 
				"    \"totalprice\" : 111,\r\n" + 
				"    \"depositpaid\" : true,\r\n" + 
				"    \"bookingdates\" : {\r\n" + 
				"        \"checkin\" : \"2021-01-01\",\r\n" + 
				"        \"checkout\" : \"2021-02-02\"\r\n" + 
				"    },\r\n" + 
				"    \"additionalneeds\" : \"Breakfast\"\r\n" + 
				"}");
		
		
		
		
		//Hit Request and get the response
		Response response=httpRequest.post();
		
		//validate the response
		//ValidatableResponse validatebleResponse=response.then().statusCode(200);
		//System.out.println("Response Status is : "+ validatebleResponse.toString());
		int statusCode=response.getStatusCode();
		System.out.println(statusCode);
		
		Assert.assertEquals(200,statusCode,  "Failed");
	

	}
}
