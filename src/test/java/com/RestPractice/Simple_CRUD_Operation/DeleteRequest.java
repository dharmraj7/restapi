//this is the delete functionality devloped by Dharmraj
package com.RestPractice.Simple_CRUD_Operation;

import org.testng.annotations.Test;

import io.restassured.RestAssured;

public class DeleteRequest {

	@Test
	public void getDelete() {
		
		RestAssured
		.given()
		.log().all()
		.baseUri("https://restful-booker.herokuapp.com/")
		.basePath("booking/11")
		.header("Authorization","Basic YWRtaW46cGFzc3dvcmQxMjM=")
		.when()
		.delete()
		.then()
		.log()
		.all()
		.statusCode(201);
	}
	
}
