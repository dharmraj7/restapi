package com.RestPractice;
 
import org.hamcrest.Matcher;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.module.jsv.JsonSchemaValidator; 
public class JSON_SchemaExample_PostRequest {


	public  static void main(String args[])
	{
		RestAssured.
		given().log().all().
			baseUri("https://restful-booker.herokuapp.com/").
				basePath("booking").
					body("{\r\n" + 
							"    \"firstname\" : \"Jim\",\r\n" + 
							"    \"lastname\" : \"Brown\",\r\n" + 
							"    \"totalprice\" : 111,\r\n" + 
							"    \"depositpaid\" : true,\r\n" + 
							"    \"bookingdates\" : {\r\n" + 
							"        \"checkin\" : \"2018-01-01\",\r\n" + 
							"        \"checkout\" : \"2019-01-01\"\r\n" + 
							"    },\r\n" + 
							"    \"additionalneeds\" : \"Breakfast\"\r\n" + 
							"}")
					.contentType(ContentType.JSON).when().post()
					.then()
					.log()
					.all()
						.statusCode(200)
						//.body(matchesJsonSchemaInClasspath("\\resources\\JSONSchemaExample_CreateBooking.json"));
						.body(JsonSchemaValidator.matchesJsonSchemaInClasspath("JSONSchemaExample_CreateBooking.json"));
		System.out.println("File Found");
	}

	 
}

