package com.RestPractice.Payload;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import io.restassured.RestAssured;

public class ComplexPayload_ArrayAndMap {

	//Complex JSON Payload with using List and Map
	public static void main(String[] args) {
		// Final List of payload
		List<Map<String, Object>> list_Finalpayload = new ArrayList<Map<String, Object>>();
		//Main json payload add into  map
		Map<String, Object> first_JsonObjectPayload = new LinkedHashMap<String, Object>();
		first_JsonObjectPayload.put("id", "1");
		first_JsonObjectPayload.put("FirstName", "Nick");
		first_JsonObjectPayload.put("LastName", "lavis");
		first_JsonObjectPayload.put("Email", "nick@gmail.com");
		first_JsonObjectPayload.put("Gender", "Male");

		// List<String>mobileNumber=new ArrayList<String>();
		// mobileNumber.add("9867766767");
		// mobileNumber.add("9823993229");
		List<String> list_MobileNumber = Arrays.asList("9822332323", "9877667788");
		first_JsonObjectPayload.put("Mobile", list_MobileNumber);

		Map<String, Object> skill_MapSet1 = new HashMap<String, Object>();
		skill_MapSet1.put("Name", "Core Java");
		skill_MapSet1.put("Proficiency", "Excellent");
		
		first_JsonObjectPayload.put("Skills", skill_MapSet1);
		
		list_Finalpayload.add(first_JsonObjectPayload);
		
		//Second JSON Object
		
		Map<String, Object> second_JsonObectPayload = new LinkedHashMap<String, Object>();
		second_JsonObectPayload.put("id", "2");
		second_JsonObectPayload.put("FirstName", "Mike");
		second_JsonObectPayload.put("LastName", "Charles");
		second_JsonObectPayload.put("Email", "Mike@gmail.com");
		second_JsonObectPayload.put("Gender", "Male");
		
		List<Map<String, Object>> skills_List2=new ArrayList<Map<String,Object>>();
		skills_List2.add(skill_MapSet1);
		
		Map<String, Object> skills_MapSet2=new LinkedHashMap<String, Object>();
		skills_MapSet2.put("Name", ".Net Core");
		skills_MapSet2.put("Proficiency", "Excellent");
		List<String> list_AllCertification = Arrays.asList("Mircosoft","TCDER");
		
		skills_MapSet2.put("Certification",list_AllCertification);
		skills_List2.add(skills_MapSet2);
		second_JsonObectPayload.put("Skills", skills_List2);
		list_Finalpayload.add(second_JsonObectPayload);
		RestAssured
			.given()
				.log()
				.all()
				.body(list_Finalpayload)
				.get();
	}
	

}
