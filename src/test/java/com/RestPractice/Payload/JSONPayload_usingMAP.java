package com.RestPractice.Payload;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import io.restassured.RestAssured;

public class JSONPayload_usingMAP {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Simple JSON payload example:");
		//simpleJsonPaylod_map();
		System.out.println();
		System.out.println("Nested JSOn Payload object");
		//nestedJsonPaylod_map();
		
		jsonArrayPayload_usingMap();
		
	}
	public static void simpleJsonPaylod_map()
	{
		Map<Object, Object> jsonObjectPayload = new LinkedHashMap<Object, Object>();

		jsonObjectPayload.put("id", "1");
		jsonObjectPayload.put("FirstName", "Nick");
		jsonObjectPayload.put("LastName", "lavis");
		jsonObjectPayload.put("Salary", "123.34");

		RestAssured.given().log().all().body(jsonObjectPayload).get();
	}
	
	public static void nestedJsonPaylod_map()
	{
		Map<String, Object> jsonObjectPayload = new LinkedHashMap<String, Object>();

		jsonObjectPayload.put("id", "1");
		jsonObjectPayload.put("FirstName", "Nick");
		jsonObjectPayload.put("LastName", "lavis");
		jsonObjectPayload.put("Salary", "123.34");
		
		Map<String, Object> addressPayload = new LinkedHashMap<String, Object>();
		addressPayload.put("Street", "abc Road");
		addressPayload.put("City", "Pune");
		addressPayload.put("State", "Maharashtra");
		
		jsonObjectPayload.put("address", addressPayload);
		
		RestAssured.given().log().all().body(jsonObjectPayload).get();
	}
	public static void jsonArrayPayload_usingMap() {
		Map<String, Object> jsonObjectPayload = new LinkedHashMap<String, Object>();

		jsonObjectPayload.put("id", "1");
		jsonObjectPayload.put("FirstName", "Nick");
		jsonObjectPayload.put("LastName", "lavis");
		jsonObjectPayload.put("Salary", "123.34");
		
		Map<String, Object> jsonObjectPayload2 = new LinkedHashMap<String, Object>();

		jsonObjectPayload2.put("id", "2");
		jsonObjectPayload2.put("FirstName", "Vince");
		jsonObjectPayload2.put("LastName", "Brown");
		jsonObjectPayload2.put("Salary", "200.34");
		
		
		Map<String, Object> addressPayload = new LinkedHashMap<String, Object>();
		addressPayload.put("Street", "LY Road");
		addressPayload.put("City", "New Mumbai");
		addressPayload.put("State", "Maharashtra");
		
		jsonObjectPayload.put("address", addressPayload);
		jsonObjectPayload2.put("address", addressPayload);
		
		List<Map<String, Object>> allRecord=new ArrayList<Map<String, Object>>();
		allRecord.add(jsonObjectPayload);
		allRecord.add(jsonObjectPayload2);
		RestAssured.given().log().all().body(allRecord).get();
	}

}
