package com.RestPractice;

public class Test1 {

    private String date;
    private String account;
    private String cname;

    public Test1(String date, String account, String cname) {
        this.date = date;
        this.account = account;
        this.cname = cname;
    }

    @Override
    public String toString() {
        return "Test1{" +
                "date='" + date + '\'' +
                ", account='" + account + '\'' +
                ", cname='" + cname + '\'' +
                '}';
    }

    public Test1() {

    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }
}
