package com.RestPractice;

import org.testng.collections.CollectionUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class OriginClass {

    public static <test1> void main(String args[]) throws IOException {


        ArrayList<String> lines = new ArrayList<>(Files.readAllLines(Paths.get("src/main/resource/TestData.txt")));
        List<Test1> testData = new ArrayList<Test1>();
        for (String data : lines) {
            String[] ss = data.split("[|]");
            List<String> win1 = Arrays.asList(ss);
            //System.out.println( win1);
            testData.add(constructList(win1));
        }
        System.out.println(testData);
    }

    public static Test1 constructList(List<String> l1) {
        Test1 test1 = new Test1();

        //System.out.println("****" + l1);
        if (!l1.isEmpty()) {
            //Test1 test2 = new Test1(l1.get(0), l1.get(1), l1.get(2));
            if (l1.get(0) != null) {
                test1.setDate(l1.get(0));
            }
            if (l1.get(1) != null) {
                test1.setAccount(l1.get(1));
            }
            if (l1.get(2) != null) {
                test1.setCname(l1.get(2));
            }
        }
        return test1;
    }
}
